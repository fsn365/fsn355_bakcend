import { Transport } from '@nestjs/microservices';
import { Web3Server } from './services';

export const config = () => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },

  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },

  app: {
    name: 'Server',
    port: process.env['app_port'] || 8080,
  },

  redis: {
    name: 'server',
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379,
    ttl: 4,
  },

  rpc: process.env.rpc_url,

  ['service:tx-worker']: {
    transport: Transport.TCP,
    options: {
      host: process.env['service_tx_worker_host'],
      port: +process.env['service_tx_worker_port'],
    },
  },

  ['service:app']: {
    transport: Transport.TCP,
    options: {
      host: process.env['service_server_host'],
      port: +process.env['service_server_port'],
    },
  },

  ['service:web3']: {
    strategy: new Web3Server(process.env.wss_url),
  },
});

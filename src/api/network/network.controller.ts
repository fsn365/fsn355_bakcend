import { Controller, Get } from '@nestjs/common';
import { INetworkSummary } from '../../models';
import { NetworkService } from './network.service';

@Controller('network')
export class NetworkController {
  constructor(private service: NetworkService) {}

  @Get()
  async getNetworkSummary(): Promise<INetworkSummary> {
    return this.service.getNetworkSummary();
  }

  @Get('latest')
  async getLBksTxs(): Promise<{ bks: any[]; txs: any[] }> {
    return this.service.getLBKsTxs();
  }
}

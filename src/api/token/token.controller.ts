import { Controller, Get, Query, Param, UsePipes } from '@nestjs/common';
import { TokenService } from './token.service';
import {
  HashPipe,
  TokenHoldersQueryPipe,
  TokensHoldersQueryDto,
  PageQueryDto,
  PageQueryPipe,
  QueryCmdPipe,
  QueryCmdDto,
} from '../../common';

@Controller('tokens?')
export class TokenController {
  constructor(private readonly service: TokenService) {}

  @Get()
  @UsePipes(PageQueryPipe)
  getTokens(@Query() query: PageQueryDto) {
    return this.service.getTokens(query);
  }

  @Get(':hash')
  @UsePipes(new HashPipe('token'))
  getToken(@Param('hash') hash: string) {
    return this.service.getToken(hash);
  }

  @Get(':hash/holders')
  @UsePipes(new HashPipe('token'))
  @UsePipes(TokenHoldersQueryPipe)
  getTokenHolders(
    @Param('hash') hash: string,
    @Query() query: TokensHoldersQueryDto,
  ) {
    return this.service.getTokenHolders(hash, query);
  }

  @Get(':hash/txs')
  @UsePipes(new HashPipe('token'))
  @UsePipes(QueryCmdPipe)
  getTokenTransactions(
    @Param('hash') hash: string,
    @Query() query: QueryCmdDto,
  ) {
    return this.service.getTokenTxs(hash, query);
  }
}

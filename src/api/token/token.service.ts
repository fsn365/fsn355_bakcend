import { Injectable } from '@nestjs/common';
import { TokensHoldersQueryDto, PageQueryDto, QueryCmdDto } from '../../common';
import { PgTokenService, PgTxsService } from '../../shared';

@Injectable()
export class TokenService {
  constructor(private service: PgTokenService, private tx: PgTxsService) {}

  getTokens(query: PageQueryDto): Promise<any[]> {
    return this.service.getTokens(query);
  }

  getToken(hash: string): Promise<any> {
    return this.service.getToken(hash);
  }

  getTokenHolders(hash: string, query: TokensHoldersQueryDto) {
    return this.service.getTokenHolders(hash, query);
  }

  getTokenTxs(hash: string, query: QueryCmdDto) {
    return this.tx.getTokensTxs(hash, query);
  }
}

import {
  Controller,
  Get,
  Param,
  CacheTTL,
  Query,
  UsePipes,
} from '@nestjs/common';
import { AddressPipe } from './address.pipe';
import { AddressService } from './address.service';
import {
  AddressId,
  IAddress,
  IAddressAsset,
  IAddressTlAsset,
  ITx,
} from '../../models';
import { QueryCmdDto, QueryCmdPipe } from '../../common';

@Controller('address')
@CacheTTL(6)
export class AddressController {
  constructor(private readonly service: AddressService) {}

  @Get('names')
  getLabeledAddresses(): Promise<object> {
    return this.service.getLabeledAddresses();
  }

  @Get(':address')
  getAddressOverview(
    @Param('address', AddressPipe) address: AddressId,
  ): Promise<IAddress> {
    return this.service.getAddressOverview(address);
  }

  @Get(':address/assets')
  getAddressAssets(
    @Param('address', AddressPipe) address: AddressId,
  ): Promise<IAddressAsset[]> {
    return this.service.getAddressAssets(address);
  }

  @Get(':address/tlassets')
  getAddressTlAssets(
    @Param('address', AddressPipe) address: AddressId,
  ): Promise<IAddressTlAsset[]> {
    return this.service.getAddressTlAssets(address);
  }

  @Get(':address/txs')
  @UsePipes(QueryCmdPipe)
  getAddressTxs(
    @Param('address', AddressPipe) address: AddressId,
    @Query() query: QueryCmdDto,
  ): Promise<ITx[]> {
    return this.service.getAddressTxs(address, query);
  }
}

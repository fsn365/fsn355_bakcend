import { Injectable } from '@nestjs/common';
import { PgAddressService, PgTxsService } from '../../shared';

import { QueryCmdDto } from '../../common';

import {
  AddressId,
  IAddress,
  IAddressTlAsset,
  ITx,
  IAddressAsset,
} from '../../models';

@Injectable()
export class AddressService {
  constructor(private tx: PgTxsService, private address: PgAddressService) {}

  async getLabeledAddresses(): Promise<any> {
    return this.address.getLabeledAddresses();
  }

  async getAddressOverview(addressId: AddressId): Promise<IAddress> {
    const address: string = await this.getAddressHash(addressId);
    return this.address.getAddressOverview(address);
  }

  async getAddressAssets(addressId: AddressId): Promise<IAddressAsset[]> {
    const address: string = await this.getAddressHash(addressId);
    return this.address.getAddressAssets(address);
  }

  async getAddressTlAssets(addressId: AddressId): Promise<IAddressTlAsset[]> {
    const address: string = await this.getAddressHash(addressId);
    return this.address.getAddressTlAssets(address);
  }

  async getAddressTxs(
    addressId: AddressId,
    query: QueryCmdDto,
  ): Promise<ITx[]> {
    const address: string = await this.getAddressHash(addressId);
    return this.tx.getAddressTxs(address, query);
  }

  private async getAddressHash(addressId: AddressId): Promise<string> {
    if (typeof addressId === 'string') return addressId;
    return this.address.getAddressByUSAN(addressId);
  }
}

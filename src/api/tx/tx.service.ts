import { Injectable } from '@nestjs/common';
import { QueryCmdDto } from '../../common/pipes';
import { MongoService } from '../../shared/mongo';
import { PgTxsService } from '../../shared';

import { ITx } from '../../models';

@Injectable()
export class TxService {
  constructor(private mongo: MongoService, private pg: PgTxsService) {}

  async getTxByHash(hash: string) {
    return Promise.all([
      this.pg.getTxByHash(hash),
      this.mongo.getTxByHash(hash),
    ]).then(data => {
      const [tx, rawTx] = data;
      return { ...rawTx, ...tx };
    });
  }

  getTxs(query: QueryCmdDto): Promise<ITx[]> {
    return this.pg.getTxs(query);
  }
}

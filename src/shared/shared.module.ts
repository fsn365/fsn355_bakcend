import { Module } from '@nestjs/common';
import { HelperModule } from './helper';
import { PgModule } from './pg';
import { MongoModule } from './mongo';

@Module({
  imports: [HelperModule, PgModule, MongoModule],
})
export class SharedModule {}

import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import {
  PageQueryDto,
  TokensHoldersQueryDto,
  CustomLogger,
} from '../../common';
import { IToken } from '../../models';

@Injectable()
export class PgTokenService extends CustomLogger {
  constructor(@InjectKnex() private readonly knex: Knex) {
    super('Server:PgTokenService');
  }

  getTokens(query: PageQueryDto, type?: number): Promise<IToken[]> {
    let table = 'assets';
    if (type === 1) table = 'erc20';

    this.logInfo({ method: 'getTokens', data: { table, type, ...query } });

    const { page, size } = query;
    const offset = page * size;

    return this.knex(table)
      .select('*')
      .offset(offset)
      .limit(size)
      .then(records => records.map(record => this.cleanToken(record)));
  }

  getToken(hash: string): Promise<IToken> {
    let table = 'assets';
    if (hash.length === 42) table = 'erc20';

    this.logInfo({ method: 'getToken', data: { table, token: hash } });

    return this.knex(table)
      .select('*')
      .where({ hash })
      .limit(1)
      .first()
      .then(record => {
        if (!record) throw new BadRequestException(`Can't find token ${hash}.`);
        else return this.cleanToken(record);
      });
  }

  async getTokensTxCount(token: string): Promise<number> {
    let table = 'assets';
    if (token.length === 42) table = 'erc20';

    this.logInfo({ method: 'getTokensTxCount', data: { table, token } });

    return this.knex(table)
      .select('txs')
      .where({ hash: token })
      .limit(1)
      .first()
      .then(record => {
        if (record) return record.txs;
        return 0;
      });
  }

  // url example: https://etherscan.io/token/0xdac17f958d2ee523a2206206994597c13d831ec7#balances
  getTokenHolders(
    token: string,
    query: TokensHoldersQueryDto,
  ): Promise<IToken[]> {
    let sort: string = query.sort;
    let table = 'address_assets';
    if (token.length === 42) {
      sort = 'qty';
      table = 'address_erc20_assets';
    }

    this.logInfo({
      method: 'getTokenHolders',
      data: { table, ...query, sort },
    });

    const { page, size } = query;
    const offset = page * size;
    if (offset > 1000) {
      throw new BadRequestException(`We only provide top 1000 holders.`);
    }

    return this.knex(table)
      .select('asset', 'address', 'label', 'qty', 'qty_in', 'qty_own')
      .where({ asset: token })
      .leftJoin(`address`, 'hash', `${table}.address`)
      .orderBy(sort, `desc`)
      .offset(offset)
      .limit(size);
  }

  private cleanToken(tokenData: any): IToken {
    const token = { ...tokenData };
    delete token.id;
    return token;
  }
}

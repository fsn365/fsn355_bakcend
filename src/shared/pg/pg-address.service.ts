import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { IAddress, IAddressTlAsset, IAddressFsnBalance } from '../../models';
import { CustomLogger } from '../../common';

@Injectable()
export class PgAddressService extends CustomLogger {
  constructor(@InjectKnex() private knex: Knex) {
    super('Server:PgAddressService');
  }

  public getLabeledAddresses(): Promise<object> {
    this.logInfo({ method: 'getLabeledAddresses' });

    return this.knex('address')
      .whereNotNull('label')
      .select('hash', 'label')
      .then(records => {
        if (!records.length) return {};
        const map = {};
        records.map(record => (map[record.hash] = record.label));
        return map;
      });
  }

  public getAddressOverview(address: string): Promise<IAddress> {
    this.logInfo({
      method: 'getAddressOverview',
      data: { table: 'address', address },
    });

    const queryOverview = this.knex('address')
      .select('*')
      .where({ hash: address })
      .limit(1)
      .first();

    const queryFsnBalance = this.getAddressFSNBalance(address);

    return Promise.all([queryFsnBalance, queryOverview])
      .then(data => {
        const [overview, fsnBalance] = data;
        return { ...overview, ...fsnBalance };
      })
      .then((data: any) => {
        delete data.id;
        Object.keys(data).map((key: string) => {
          if (data[key] === null || data[key] === false) delete data[key];
        });
        return data;
      });
  }

  // ToDo: erc20 balance
  public getAddressAssets(address: string) {
    this.logInfo({
      method: 'getAddressAssets',
      data: { table: 'address_assets', address },
    });

    return this.knex('address_assets')
      .where({ address })
      .leftJoin('assets', 'assets.hash', 'address_assets.asset')
      .select('address_assets.qty', 'qty_own', 'qty_in', 'asset', 'symbol');
  }

  public getAddressTlAssets(address: string): Promise<IAddressTlAsset[]> {
    this.logInfo({
      method: 'getAddressTlAssets',
      data: { address, table: 'address_tl_assets' },
    });

    return this.knex('address_tl_assets')
      .where({ address })
      .leftJoin('assets', 'assets.hash', 'address_tl_assets.asset')
      .select('data', 'asset', 'symbol');
  }

  public getAddressByUSAN(usan: number): Promise<string> {
    this.logInfo({
      method: 'getAddressByUSAN',
      data: { usan, table: 'address' },
    });

    return this.knex('address')
      .select('hash')
      .where({ usan })
      .limit(1)
      .first()
      .then(record => {
        if (record) return record.hash;
        throw new BadRequestException(`Can't find address with usan:${usan}`);
      });
  }

  private async getAddressFSNBalance(
    address: string,
  ): Promise<IAddressFsnBalance> {
    const FSN_TOKEN =
      '0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';
    return this.knex('address_assets')
      .where({ address })
      .andWhere({ asset: FSN_TOKEN })
      .select('qty as fsn', 'qty_in as fsnIn', 'qty_own as fsnOwn')
      .limit(1)
      .first();
  }
}

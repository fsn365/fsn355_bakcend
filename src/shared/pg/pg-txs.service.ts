import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { ITx } from '../../models';
import { CustomLogger, QueryCmdDto } from '../../common';

@Injectable()
export class PgTxsService extends CustomLogger {
  constructor(@InjectKnex() private knex: Knex) {
    super('Server:PgTxService');
  }

  async getTxByHash(hash: string): Promise<ITx> {
    return this.knex('txs')
      .where({ hash })
      .limit(1)
      .first()
      .then(txData => {
        const tx = { ...txData };
        delete tx.id;
        delete tx.assets;
        return tx;
      });
  }

  async getTxs(query: QueryCmdDto): Promise<ITx[]> {
    this.logInfo({ method: 'getTxs', data: { table: 'txs', ...query } });

    const { cmd, size, anchor } = query;
    const order: string = this.getOrderByCmd(cmd);

    if (cmd === 'first' || cmd === 'last') {
      return this.knex('txs')
        .select('*')
        .orderBy('id', order)
        .limit(size)
        .then(records => records.map(record => this.cleanTx(record)))
        .then(txs => {
          if (cmd === 'last') return txs.reverse();
          return txs;
        });
    }

    const operator: string = this.getOperatorByCmd(cmd);
    return this.knex('txs')
      .select('*')
      .where('id', operator, anchor)
      .orderBy('id', 'desc')
      .limit(size)
      .then(records => records.map((record: any) => this.cleanTx(record)));
  }

  async getBlocksTxs(block: number): Promise<ITx[]> {
    this.logInfo({ method: 'getBlocksTxs', data: { block } });

    return this.knex('txs')
      .where({ block })
      .then(records => records.map(record => this.cleanTx(record)));
  }

  async getTokensTxs(token: string, query: QueryCmdDto): Promise<ITx[]> {
    let table = 'txs';
    if (token.length === 42) table = 'erc20_txs';

    this.logInfo({ method: 'getTokensTxs', data: { token, table, ...query } });

    const { cmd, size } = query;
    const order = this.getOrderByCmd(cmd);

    const filter = ['assets', JSON.stringify([token])];

    if (cmd === 'first' || cmd === 'last') {
      return this.knex(table)
        .whereRaw(`?? @> ?::jsonb`, filter)
        .orderBy('id', order)
        .limit(size)
        .then(records => records.map((record: any) => this.cleanTx(record)))
        .then(txs => {
          if (cmd === 'last') return txs.reverse();
          return txs;
        });
    }

    const anchor = query.anchor;
    const operator = this.getOperatorByCmd(cmd);
    return this.knex(table)
      .select('*')
      .whereRaw(`?? @>?::jsonb`, filter)
      .andWhere('id', operator, anchor)
      .orderBy('id', 'desc')
      .limit(size)
      .then(records => records.map((record: any) => this.cleanTx(record)));
  }

  async getAddressTxs(address: string, query: QueryCmdDto): Promise<ITx[]> {
    const { cmd, size, anchor } = query;

    this.logInfo({
      method: 'getAddressTxs',
      data: { address, table: 'txs', ...query },
    });

    const order: string = this.getOrderByCmd(cmd);
    if (cmd === 'last' || cmd === 'first') {
      return this.knex('txs')
        .select('*')
        .where(function() {
          return this.where('sender', address).orWhere('receiver', address);
        })
        .orderBy('id', order)
        .limit(size)
        .then(records => records.map((record: any) => this.cleanTx(record)));
    }

    const operator: string = this.getOperatorByCmd(cmd);
    return this.knex('txs')
      .select('*')
      .where(function() {
        return this.where('sender', address).orWhere('receiver', address);
      })
      .andWhere('id', operator, anchor)
      .limit(size)
      .then(records => records.map(record => this.cleanTx(record)));
  }

  private cleanTx(txData: any): ITx {
    const { data = null, ...others } = txData;
    delete others.assets;
    if (data) {
      return { ...others, data };
    }
    return others;
  }

  private getOrderByCmd(cmd: string): string {
    let order: string;
    if (cmd === 'first' || cmd === 'next') order = 'desc';
    if (cmd === 'last' || cmd == 'prev') order = 'asc';
    return order;
  }

  private getOperatorByCmd(cmd: string): string {
    let operator = '>';
    if (cmd === 'next') operator = '<';
    if (cmd === 'prev') operator = '>';
    return operator;
  }
}
